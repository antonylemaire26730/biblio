package fr.anto.onepiececollection

class BookModel (
    val name: String ="tulipe",
    val description: String = "petite description",
    val page: String = "120",
    val imageUrl: String = "https://unsplash.com/photos/nGrfKmtwv24",
    var liked: Boolean = false
)
