package fr.anto.onepiececollection.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import fr.anto.onepiececollection.BookModel
import fr.anto.onepiececollection.BookRepository.Singleton.bookList
import fr.anto.onepiececollection.MainActivity
import fr.anto.onepiececollection.R
import fr.anto.onepiececollection.adapter.BookAdapter
import fr.anto.onepiececollection.adapter.BookItemDecoration

class HomeFragment(
    private val context: MainActivity
): Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       val view = inflater?.inflate(R.layout.fragment_home , container , false)





        //recuperer le recyclerview
        val horizontalRecyclerView = view.findViewById<RecyclerView>(R.id.horizontal_recycler_view)
        horizontalRecyclerView.adapter = BookAdapter(context,bookList,R.layout.item_horizontal_book)

        //recuper le second recyclerview
        val verticalRecyclerView = view.findViewById<RecyclerView>(R.id.vertical_recycler_view)
        verticalRecyclerView.adapter = BookAdapter(context, bookList,R.layout.item_vertical_book)
        verticalRecyclerView.addItemDecoration(BookItemDecoration())

        return view
    }
}