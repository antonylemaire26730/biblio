package fr.anto.onepiececollection

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.anto.onepiececollection.fragments.HomeFragment


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //charger notre  BookRepositoru
        val repo = BookRepository()

        //mettre a jour la liste de livre
        repo.updateData {

            // injecter le fragment dans notre boite fragment  (fragment_container)
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, HomeFragment(this))
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}