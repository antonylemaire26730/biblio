package fr.anto.onepiececollection.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.anto.onepiececollection.BookModel
import fr.anto.onepiececollection.MainActivity
import fr.anto.onepiececollection.R

class BookAdapter(
        private val context: MainActivity,
        private val bookList: List<BookModel>,
        private val layoutId: Int
) : RecyclerView.Adapter<BookAdapter.ViewHolder>() {

    // boite pour ranger tout les composants à controler
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        //image du livre
        val bookImage = view.findViewById<ImageView>(R.id.image_item)
        val bookName:TextView? = view.findViewById(R.id.name_item)
        val bookPage: TextView? = view.findViewById(R.id.description_item)
        val starIcon = view.findViewById<ImageView>(R.id.start_icon)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(layoutId, parent , false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = bookList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //recuperer les infos du livre
        val currentBook = bookList[position]

        // utiliser glide pour recuperer limage a partir de son lien et l'ajouter a note composant
        Glide.with(context).load(Uri.parse(currentBook.imageUrl)).into(holder.bookImage)

        //mettre à jour le nom du livre
        holder.bookName?.text = currentBook.name

        //mettre a jour la page du livre
        holder.bookPage?.text = currentBook.page

        //verifier si la plante a était liker
        if (currentBook.liked){
            holder.starIcon.setImageResource(R.drawable.ic_star)
        }
        else {
            holder.starIcon.setImageResource(R.drawable.ic_unstar)
        }


    }
}