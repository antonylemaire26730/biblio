package fr.anto.onepiececollection

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import fr.anto.onepiececollection.BookRepository.Singleton.bookList
import fr.anto.onepiececollection.BookRepository.Singleton.databaseRef
import javax.security.auth.callback.Callback

class BookRepository {

    object Singleton {
        // se connecté a la reference book
        val databaseRef = FirebaseDatabase.getInstance().getReference("book")

        //creer une liste qui va contenir mes books
        val bookList = arrayListOf<BookModel>()
    }
    fun updateData(callback: () -> Unit ) {
        //absorber des données depuos la databaseRef -> liste de livre
        databaseRef.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                //retirer les anciennes
                bookList.clear()
                //recolter la liste
                for (ds in snapshot.children){
                    //construire un objet
                    val book = ds.getValue(BookModel::class.java)

                    //verifier que le livre n'est pas null
                    if (book != null){
                        // ajouter la plant a notre liste
                        bookList.add(book)
                    }
                }
                //actionner le callback
                callback()
            }

        })
    }

}